AOS.init({once:true});

var swiper = new Swiper('.swiper-container-head', {
    spaceBetween: 40,
    navigation: {
        nextEl: '.sbn-h',
        prevEl: '.sbp-h',
    },
    pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
    }
});

$(document).ready(function () {

    DG.then(function () {
        map = DG.map('map', {
            center: [55.788682, 37.709351],
            zoom: 16
        });
        DG.marker([55.788682, 37.709351]).addTo(map);
    });

    var swiper = new Swiper('.swiper-container-feedback', {
        slidesPerView: 4,
        spaceBetween: 40,
        navigation: {
            nextEl: '.sbn',
            prevEl: '.sbp',
        },
        breakpoints: {
            768: {
                slidesPerView: 1,
                spaceBetween: 30
            }
        }
    });

});
